package yourfunworldstudios.socialconquer.GuildMechanics;
public class GuildMechanics
    extends java.lang.Object
     implements org.bukkit.event.Listener{

static java.util.logging.Logger log= null;
public static yourfunworldstudios.socialconquer.GuildMechanics.GuildMechanics instance= null;
public static final org.bukkit.Color DEFAULT_LEATHER_COLOR= null;
public static org.bukkit.inventory.ItemStack guild_dye= null;
public static org.bukkit.inventory.ItemStack color_selector_divider= null;
public org.bukkit.inventory.ItemStack gray_button;
public static org.bukkit.inventory.ItemStack guild_emblem= null;
private static final java.lang.String ALPHA_NUM= null;
private static java.security.SecureRandom random= null;
public static java.util.HashMap guild_colors= null;
public static java.util.concurrent.ConcurrentHashMap guild_creation_name_check= null;
public static java.util.HashMap guild_member_server= null;
public static java.util.HashMap guild_map= null;
public static java.util.HashMap guild_motd= null;
public static java.util.HashMap guild_bio_dynamic= null;
public static java.util.HashMap guild_bio= null;
public static java.util.HashMap guild_map_clone= null;
public static java.util.HashMap guild_server= null;
public static java.util.HashMap guild_handle_map= null;
public static java.util.HashMap player_guilds= null;
public static java.util.HashMap player_guild_rank= null;
public static java.util.HashMap guild_invite= null;
public static java.util.HashMap guild_inviter= null;
public static java.util.HashMap guild_invite_time= null;
public static java.util.HashMap guild_creation= null;
public static java.util.HashMap guild_creation_data= null;
public static java.util.concurrent.ConcurrentHashMap guild_creation_npc_location= null;
public static java.util.HashMap guild_creation_code= null;
public static java.util.List guild_only= null;
public static java.util.List guild_quit_confirm= null;

/** KAYABA WAS HERE! ~ Generated by Kayaba's API Stub Tool :> */
public GuildMechanics() {
super ();
 }


/** Empty implementation */
public void setColor(org.bukkit.inventory.ItemStack p0, org.bukkit.Color p1) {
}

/** Empty implementation */
public void onEnable() {
}

/** Empty implementation */
public void onPlayerJoin(org.bukkit.event.player.PlayerJoinEvent p0) {
}

/** Empty implementation */
public void onDisable() {
}

/** Empty implementation */
public static boolean inGuild(java.lang.String p0) {
return false;
}

/** Empty implementation */
public void onAsyncPlayerChatEvent(org.bukkit.event.player.AsyncPlayerChatEvent p0) {
}

/** Empty implementation */
public void onInventoryClick(org.bukkit.event.inventory.InventoryClickEvent p0) {
}

/** Empty implementation */
public void onEntityDamageByEntityEvent(org.bukkit.event.entity.EntityDamageEvent p0) {
}

/** Empty implementation */
public static java.lang.String getGuild(java.lang.String p0) {
return null;
}

/** Empty implementation */
public static int getRankNum(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public static void sendMessageToGuild(org.bukkit.entity.Player p0, java.lang.String p1) {
}

/** Empty implementation */
public static java.lang.String getGuildPrefix(java.lang.String p0) {
return null;
}

/** Empty implementation */
public void onPlayerQuit(org.bukkit.event.player.PlayerQuitEvent p0) {
}

/** Empty implementation */
public static java.lang.String nextSessionId() {
return null;
}

/** Empty implementation */
public static void updateGuildTabList(org.bukkit.entity.Player p0) {
}

/** Empty implementation */
public static void setLocalGuildMOTD(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static java.util.List getOnlineGuildMembers(java.lang.String p0) {
return null;
}

/** Empty implementation */
public static boolean downloadGuildDataSQL(java.lang.String p0, boolean p1) {
return false;
}

/** Empty implementation */
public static void setGuildRank(java.lang.String p0, int p1) {
}

/** Empty implementation */
public static void addPlayerToGuild(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static void updateGuildSQL(java.lang.String p0) {
}

/** Empty implementation */
public static void setLocalGuildBIO(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static java.lang.String[] getGuildMembers(java.lang.String p0) {
return null;
}

/** Empty implementation */
public static void leaveGuild(java.lang.String p0) {
}

/** Empty implementation */
public static void leaveGuild(java.lang.String p0, java.lang.String p1, boolean p2) {
}

/** Empty implementation */
public static void promoteToOwnerInOwnGuild(org.bukkit.entity.Player p0, java.lang.String p1) {
}

/** Empty implementation */
public static void sendGuildMessageCrossServer(java.lang.String p0) {
}

/** Empty implementation */
public static void setPlayerGuildSQL(java.lang.String p0, java.lang.String p1, boolean p2) {
}

/** Empty implementation */
public static void setGuildBannerSQL(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static void promoteToOwnerInSpecificGuild(org.bukkit.entity.Player p0, java.lang.String p1, java.lang.String p2) {
}

/** Empty implementation */
public static int getTotalCoOwnersCount(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public static int getTotalOfficerCount(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public static void clearGuildTabList(org.bukkit.entity.Player p0, boolean p1) {
}

/** Empty implementation */
public static java.lang.String getPlayerGuildSQL(java.lang.String p0) {
return null;
}

/** Empty implementation */
public void onPlayerInteractEntity(org.bukkit.event.player.PlayerInteractEntityEvent p0) {
}

/** Empty implementation */
public static int getOnlineGuildCount(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public static void processGuildExistRequest(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static void removeGuildColors(org.bukkit.entity.Player p0, org.bukkit.inventory.Inventory p1) {
}

/** Empty implementation */
public static java.lang.String getUpgradeAuthenticationCode(java.lang.String p0) {
return null;
}

/** Empty implementation */
public void onColorPickerClose(org.bukkit.event.inventory.InventoryCloseEvent p0) {
}

/** Empty implementation */
public static java.lang.String getLocalServerName() {
return null;
}

/** Empty implementation */
public static int getOnlineCoOwnersCount(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public boolean doesGuildHandlerExistLocal(java.lang.String p0) {
return false;
}

/** Empty implementation */
public boolean doesGuildExistLocal(java.lang.String p0) {
return false;
}

/** Empty implementation */
public static int getGuildMemberCount(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public static int getOnlineOfficerCount(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public static void processGuildTagExistRequest(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static boolean inSpecificGuild(java.lang.String p0, java.lang.String p1) {
return false;
}

/** Empty implementation */
public static boolean isGuildCoOwner(java.lang.String p0) {
return false;
}

/** Empty implementation */
public static boolean areGuildies(java.lang.String p0, java.lang.String p1) {
return false;
}

/** Empty implementation */
public static boolean isGuildOfficer(java.lang.String p0) {
return false;
}

/** Empty implementation */
public static void inviteToGuild(org.bukkit.entity.Player p0, org.bukkit.entity.Player p1) {
}

/** Empty implementation */
public static boolean isGuildLeader(java.lang.String p0) {
return false;
}

/** Empty implementation */
public static int getGuildRank(java.lang.String p0, java.lang.String p1) {
return 0;
}

/** Empty implementation */
public static int getGuildRank(java.lang.String p0) {
return 0;
}

/** Empty implementation */
public static void demoteCoOwner(java.lang.String p0, org.bukkit.entity.Player p1) {
}

/** Empty implementation */
public static void demoteOfficer(java.lang.String p0, org.bukkit.entity.Player p1) {
}

/** Empty implementation */
public static void promoteToCoOwner(java.lang.String p0, org.bukkit.entity.Player p1) {
}

/** Empty implementation */
public static void promoteToOfficer(java.lang.String p0, org.bukkit.entity.Player p1) {
}

/** Empty implementation */
public void guildMemberQuit(org.bukkit.entity.Player p0) {
}

/** Empty implementation */
public static void setGuildBIO(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public void guildMemberJoin(org.bukkit.entity.Player p0) {
}

/** Empty implementation */
public static void setGuildMOTD(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public org.bukkit.Color getGuildColor(java.lang.String p0) {
return null;
}

/** Empty implementation */
public void createGuildSQL(java.lang.String p0, java.lang.String p1, int p2, java.lang.String p3) {
}

/** Empty implementation */
public boolean isColoredArmor(org.bukkit.inventory.ItemStack p0) {
return false;
}

/** Empty implementation */
public static void setupGuildTeam(java.lang.String p0) {
}

/** Empty implementation */
public static org.bukkit.inventory.ItemStack makeTradeable(org.bukkit.inventory.ItemStack p0) {
return null;
}

/** Empty implementation */
public void onItemDrop(org.bukkit.event.player.PlayerDropItemEvent p0) {
}

/** Empty implementation */
public boolean isGuildDye(org.bukkit.inventory.ItemStack p0) {
return false;
}

/** Empty implementation */
public void onColorPicker(org.bukkit.event.inventory.InventoryClickEvent p0) {
}

/** Empty implementation */
public void onEXPBottleThrow(org.bukkit.event.player.PlayerInteractEvent p0) {
}

/** Empty implementation */
public static java.util.List getGuildOfficers(java.lang.String p0) {
return null;
}

/** Empty implementation */
public static java.lang.String getGuildOwner(java.lang.String p0) {
return null;
}

/** Empty implementation */
public static java.util.List getGuildCoOwners(java.lang.String p0) {
return null;
}

/** Empty implementation */
public static void deleteGuildSQL(java.lang.String p0) {
}

/** Empty implementation */
public static void setMOTDSQL(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static void setBIOSQL(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static void setupGuildData(java.lang.String p0, java.lang.String p1) {
}

/** Empty implementation */
public static java.lang.String generateUpgradeAuthenticationCode(java.lang.String p0) {
return null;
}

}
