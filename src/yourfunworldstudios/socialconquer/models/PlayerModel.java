package yourfunworldstudios.socialconquer.models;
public class PlayerModel
    extends java.lang.Object
{

private java.lang.String name;
private java.lang.String achievements;
private long globalChatDelay;
private org.bukkit.Location deathLocation;
private long muteTime;
private yourfunworldstudios.socialconquer.holograms.Hologram chatHologram;
private java.lang.String lastReply;
private long lastPMTime;
private java.util.List buddyList;
private java.util.List localConfirmedBuddies;
private java.util.List ignoreList;
private java.util.List localConfirmedIgnores;
private java.util.List toggleList;
private long lastOnline;
private long lastBookClick;
private long rollDelay;
private int serverNum;
private int destructionWandUseCount;
private long destructionWandCooldown;
private long destructionWandLastUse;
private boolean flameTrail;
private boolean flamingArmor;
private boolean musicSprites;
private boolean demonicAura;
private boolean goldCurse;
private org.bukkit.Location musicBoxPlacement;
private org.bukkit.Location musicBoxLocation;
private long musicBoxCooldown;
private java.lang.String ecashStorage;
private int fatigueEffect;
private float energyRegenData;
private float oldEnergy;
private long lastAttack;
private boolean sprinting;
private boolean starving;
private int health;
private int thrownPotion;
private int healthData;
private int healthRegen;
private long inCombat;
private long lastEnvironmentalDamage;
private org.bukkit.Location lastHitLocation;
private int noobPlayerWarning;
private boolean combatLogger;
private boolean noobPlayer;
private yourfunworldstudios.socialconquer.HearthstoneMechanics.Hearthstone hearthstone;
private org.bukkit.Location hearthstoneLocation;
private int hearthstoneTimer;
private boolean pendingUpload;
private boolean noUpload;
private boolean beingUploaded;
private boolean locked;
private boolean firstLogin;
private boolean onlineToday;
private boolean killingSelf;
private java.util.List remotePlayerData;
private java.lang.String localPlayerIP;
private java.util.List playerIP;
private org.bukkit.inventory.Inventory playerInventory;
private org.bukkit.Location playerLocation;
private double playerHP;
private int playerLevel;
private int playerFoodLevel;
private org.bukkit.inventory.ItemStack[] playerArmorContents;
private int playerEcash;
private int playerSDaysLeft;
private java.util.List playerPortalShards;
private long playerFirstLogin;
private java.lang.String playerBio;
private org.bukkit.inventory.ItemStack playerItemInHand;
private org.bukkit.inventory.Inventory playerMuleInventory;
private long logoutTime;
private long lastSync;
private int safeLogout;
private java.lang.String toKick;
private int forumUserGroup;
private long loginTime;
private java.lang.String serverSwap;
private org.bukkit.Location serverSwapLocation;
private java.lang.String serverSwapPending;
private boolean loaded;
private java.util.List totalTiming;
private int averageTiming;
private long instanceTiming;
private java.lang.String playerInstance;
private java.util.List instanceParty;
private org.bukkit.Location savedLocationInstance;
private long processingMove;
private java.util.List armorData;
private java.util.List damageData;
private yourfunworldstudios.socialconquer.ItemMechanics.PlayerArrowReplace arrowReplace;
private int strength;
private int dexterity;
private int vitality;
private int intelligence;
private int fireResistance;
private int iceResistance;
private int poisonResistance;
private int dodge;
private int block;
private int thorn;
private int reflection;
private int itemFind;
private int gemFind;
private long lastOrbUse;
private int tier;
private java.util.List armorContents;
private boolean needUpdate;
private boolean noNegation;
private boolean processingDamageEvent;
private boolean processingProjectileEvent;
private org.bukkit.inventory.ItemStack spoofedWeapon;
private boolean processWeapon;
private java.lang.String alignment;
private int alignTime;
private java.lang.String plastHit;
private long lastHit;
private long lastPlayerAttack;
private java.util.List savedGear;
private java.lang.String lostGear;
private org.bukkit.Location savedLocation;
private int lootSpawnStep;
private java.lang.String lootSpawnData;
private org.bukkit.Location lootSpawnLocation;
private org.bukkit.Location lastOpenedLootChest;
private int itemBeingBought;
private int orbBeingBought;
private int dyeBeingBought;
private int skillBeingBought;
private int ecashItemBeingBought;
private int shardItemBeingBought;
private java.lang.String lookingIntoOfflineBank;
private int reportStep;
private int particleEffects;
private java.lang.String reportData;
private long lastUnstuck;
private int muteCount;
private int kickCount;
private int banCount;
private boolean usedStuck;
private boolean vanished;
private java.util.List bankContents;
private int bankLevel;
private java.lang.String bankUpgradeCode;
private int bank;
private int split;
private int withdraw;
private java.lang.String withdrawType;
private int mobSpawnStep;
private java.lang.String mobSpawnData;
private org.bukkit.Location mobSpawnLocation;
private long playerSlow;
private java.lang.String passiveHunter;
private long lastMobMessage;
private int mountBeingBought;
private org.bukkit.Location horseSageLocation;
private int summonMount;
private org.bukkit.Location summonLocation;
private org.bukkit.inventory.ItemStack summonItem;
private org.bukkit.entity.Entity mount;
private org.bukkit.entity.Entity mule;
private org.bukkit.inventory.Inventory muleInventory;
private java.lang.String muleItemList;
private boolean inShop;
private java.lang.String partyInvite;
private long partyInviteTime;
private yourfunworldstudios.socialconquer.PartyMechanics.Party party;
private java.lang.String partyLoot;
private int partyLootIndex;
private boolean partyOnly;
private java.lang.String rank;
private int rankForumGroup;
private java.util.List pets;
private long petSpawnDelay;
private java.util.List petEntities;
private org.bukkit.Location namingPet;
private java.lang.String petType;
private java.util.List phraseList;
private int slowMining;
private long lastSwing;
private org.bukkit.Location playerFishingSpot;
private java.util.HashMap furnaceInventory;
private boolean ignoreFurnaceOpenEvent;
private int fishCaughtCount;
private int fishHealthRegen;
private int fishEnergyRegen;
private int fishBonusDamage;
private int fishBonusArmor;
private int fishBonusBlock;
private int fishBonusLifesteal;
private int fishBonusCriticalHit;
private int currentItemBeingBought;
private org.bukkit.inventory.ItemStack itemStackBeingBought;
private int shopPage;
private java.lang.String shopCurrency;
private long recentMovement;
private org.bukkit.Location savedOutOfRealmLocation;
private java.lang.String portalMapCoords;
private org.bukkit.Location inventoryPortalMap;
private boolean hasPortal;
private long portalCooldown;
private java.lang.String realmUpgradeCode;
private java.lang.String realmPercent;
private java.lang.String realmTitle;
private int realmTier;
private int savedLevels;
private boolean realmLoadedStatus;
private long safeRealm;
private long flyingRealm;
private long realmResetCooldown;
private java.util.List buildList;
private boolean readyWorld;
private boolean corruptWorld;
private long godMode;
private int deaths;
private java.util.List kills;
private int mobKills;
private java.util.List duelStatistics;
private int inventoryUpdate;
private long warnedDurability;
private org.bukkit.inventory.ItemStack repair;
private org.bukkit.entity.Item itemRepair;
private org.bukkit.Location anvil;
private int repairState;
private java.lang.String zoneType;
private boolean recentCraft;
private boolean inInventory;
private boolean recentBlockEvent;
private int shopLevel;
private org.bukkit.block.Block inverseShop;
private org.bukkit.inventory.Inventory shopStock;
private org.bukkit.inventory.ItemStack itemBeingStocked;
private int shopServer;
private java.lang.String shopBeingBrowsed;
private java.lang.String shopUpgradeCode;
private long lastShopOpen;
private org.bukkit.inventory.Inventory collectionBin;
private boolean priceUpdateNeeded;
private boolean openingShop;
private boolean needSQLUpdate;
private java.lang.String tp;
private int tpEffect;
private org.bukkit.Location tpLocation;
private long processingMoveTeleport;
private org.bukkit.Location warp;
private org.bukkit.entity.Player trade;
private org.bukkit.entity.Player tradePartners;
private org.bukkit.inventory.Inventory tradeSecure;
private long lastInventoryClose;
private java.util.List quest;
private java.util.List completionDelay;
private boolean skipConfirm;
private boolean leaveConfirm;
private boolean enchantScroll;
private boolean onIsland;
private org.bukkit.WeatherType weather;
private long lastLocalLogin;
private yourfunworldstudios.socialconquer.LevelMechanics.PlayerLevel player_level;
private int regenFoodBonus;
private boolean isBuyingItem;
private int regenTimer;
private org.bukkit.Location teleportLoc;

/** KAYABA WAS HERE! ~ Generated by Kayaba's API Stub Tool :> */
public PlayerModel(java.lang.String p0) {
super ();
 }


/** Empty implementation */
public boolean isLocked() {
return false;
}

/** Empty implementation */
public boolean isLoaded() {
return false;
}

/** Empty implementation */
public boolean isSprinting() {
return false;
}

/** Empty implementation */
public void setSprinting(boolean p0) {
}

/** Empty implementation */
public int getHealth() {
return 0;
}

/** Empty implementation */
public void setHealth(int p0) {
}

/** Empty implementation */
public java.lang.String getAchievements() {
return null;
}

/** Empty implementation */
public void setAchievements(java.lang.String p0) {
}

/** Empty implementation */
public org.bukkit.entity.Player getPlayer() {
return null;
}

/** Empty implementation */
public java.lang.String getRank() {
return null;
}

/** Empty implementation */
public java.util.List getToggleList() {
return null;
}

/** Empty implementation */
public int getBlock() {
return 0;
}

/** Empty implementation */
public java.util.List getArmorContents() {
return null;
}

/** Empty implementation */
public void setGlobalChatDelay(long p0) {
}

/** Empty implementation */
public long getGlobalChatDelay() {
return 0;
}

/** Empty implementation */
public java.util.List getBuddyList() {
return null;
}

/** Empty implementation */
public void setBuddyList(java.util.List p0) {
}

/** Empty implementation */
public void setIgnoreList(java.util.List p0) {
}

/** Empty implementation */
public int getServerNum() {
return 0;
}

/** Empty implementation */
public long getLastPMTime() {
return 0;
}

/** Empty implementation */
public long getLastBookClick() {
return 0;
}

/** Empty implementation */
public void setServerNum(int p0) {
}

/** Empty implementation */
public void setLastBookClick(long p0) {
}

/** Empty implementation */
public void setToggleList(java.util.List p0) {
}

/** Empty implementation */
public void setRollDelay(long p0) {
}

/** Empty implementation */
public long getRollDelay() {
return 0;
}

/** Empty implementation */
public void setLastPMTime(long p0) {
}

/** Empty implementation */
public java.lang.String getLastReply() {
return null;
}

/** Empty implementation */
public java.util.List getIgnoreList() {
return null;
}

/** Empty implementation */
public void setLastReply(java.lang.String p0) {
}

/** Empty implementation */
public double getPlayerHP() {
return 0;
}

/** Empty implementation */
public java.lang.String getAlignment() {
return null;
}

/** Empty implementation */
public java.util.List getLocalConfirmedBuddies() {
return null;
}

/** Empty implementation */
public void setLastLocalLogin(long p0) {
}

/** Empty implementation */
public void setLocalConfirmedBuddies(java.util.List p0) {
}

/** Empty implementation */
public long getLastLocalLogin() {
return 0;
}

/** Empty implementation */
public java.util.List getLocalConfirmedIgnores() {
return null;
}

/** Empty implementation */
public void setLocalConfirmedIgnores(java.util.List p0) {
}

/** Empty implementation */
public org.bukkit.OfflinePlayer getOfflinePlayer() {
return null;
}

/** Empty implementation */
public yourfunworldstudios.socialconquer.LevelMechanics.PlayerLevel getPlayerLevel() {
return null;
}

/** Empty implementation */
public org.bukkit.Location getDeathLocation() {
return null;
}

/** Empty implementation */
public void setRank(java.lang.String p0) {
}

/** Empty implementation */
public java.util.List getDamageData() {
return null;
}

/** Empty implementation */
public void setPlayerHP(double p0) {
}

/** Empty implementation */
public java.util.List getArmorData() {
return null;
}

/** Empty implementation */
public boolean isFlameTrail() {
return false;
}

/** Empty implementation */
public boolean isDemonicAura() {
return false;
}

/** Empty implementation */
public int getTier() {
return 0;
}

/** Empty implementation */
public boolean isEnchantScroll() {
return false;
}

/** Empty implementation */
public int getRegenTimer() {
return 0;
}

/** Empty implementation */
public void setCombatLogger(boolean p0) {
}

/** Empty implementation */
public void setAlignment(java.lang.String p0) {
}

/** Empty implementation */
public void setRegenFoodBonus(int p0) {
}

/** Empty implementation */
public int getRegenFoodBonus() {
return 0;
}

/** Empty implementation */
public org.bukkit.inventory.ItemStack getItemStackBeingBought() {
return null;
}

/** Empty implementation */
public void setItemStackBeingBought(org.bukkit.inventory.ItemStack p0) {
}

/** Empty implementation */
public void setDeathLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setTeleportLoc(org.bukkit.Location p0) {
}

/** Empty implementation */
public org.bukkit.Location getTeleportLoc() {
return null;
}

/** Empty implementation */
public void setRegenTimer(int p0) {
}

/** Empty implementation */
public boolean isCombatLogger() {
return false;
}

/** Empty implementation */
public int getPoisonResistance() {
return 0;
}

/** Empty implementation */
public int getFireResistance() {
return 0;
}

/** Empty implementation */
public void setPlayerLevel(int p0) {
}

/** Empty implementation */
public void setPlayerLevel(yourfunworldstudios.socialconquer.LevelMechanics.PlayerLevel p0) {
}

/** Empty implementation */
public void setDexterity(int p0) {
}

/** Empty implementation */
public int getIntelligence() {
return 0;
}

/** Empty implementation */
public void setIntelligence(int p0) {
}

/** Empty implementation */
public int getStrength() {
return 0;
}

/** Empty implementation */
public int getDexterity() {
return 0;
}

/** Empty implementation */
public void updateStats() {
}

/** Empty implementation */
public int getVitality() {
return 0;
}

/** Empty implementation */
public void setVitality(int p0) {
}

/** Empty implementation */
public boolean isBuyingItem() {
return false;
}

/** Empty implementation */
public long getMuteTime() {
return 0;
}

/** Empty implementation */
public void setMuteTime(long p0) {
}

/** Empty implementation */
public int getPlayerEcash() {
return 0;
}

/** Empty implementation */
public void setToKick(java.lang.String p0) {
}

/** Empty implementation */
public boolean isMusicSprites() {
return false;
}

/** Empty implementation */
public void setLocked(boolean p0) {
}

/** Empty implementation */
public void setInCombat(long p0) {
}

/** Empty implementation */
public boolean isFirstLogin() {
return false;
}

/** Empty implementation */
public void setStarving(boolean p0) {
}

/** Empty implementation */
public int getThrownPotion() {
return 0;
}

/** Empty implementation */
public void setHearthstone(yourfunworldstudios.socialconquer.HearthstoneMechanics.Hearthstone p0) {
}

/** Empty implementation */
public void setEcashStorage(java.lang.String p0) {
}

/** Empty implementation */
public void setChatHologram(yourfunworldstudios.socialconquer.holograms.Hologram p0) {
}

/** Empty implementation */
public boolean isBeingUploaded() {
return false;
}

/** Empty implementation */
public void setFirstLogin(boolean p0) {
}

/** Empty implementation */
public void setPlayerIP(java.util.List p0) {
}

/** Empty implementation */
public int getPlayerLevels() {
return 0;
}

/** Empty implementation */
public void setOldEnergy(float p0) {
}

/** Empty implementation */
public void setHealthData(int p0) {
}

/** Empty implementation */
public void setPlayerEcash(int p0) {
}

/** Empty implementation */
public void setBeingUploaded(boolean p0) {
}

/** Empty implementation */
public yourfunworldstudios.socialconquer.holograms.Hologram getChatHologram() {
return null;
}

/** Empty implementation */
public void setThrownPotion(int p0) {
}

/** Empty implementation */
public void setMusicSprites(boolean p0) {
}

/** Empty implementation */
public int getHealthData() {
return 0;
}

/** Empty implementation */
public long getInCombat() {
return 0;
}

/** Empty implementation */
public boolean isKillingSelf() {
return false;
}

/** Empty implementation */
public java.util.List getPlayerIP() {
return null;
}

/** Empty implementation */
public void setKillingSelf(boolean p0) {
}

/** Empty implementation */
public long getLastOnline() {
return 0;
}

/** Empty implementation */
public yourfunworldstudios.socialconquer.HearthstoneMechanics.Hearthstone getHearthstone() {
return null;
}

/** Empty implementation */
public void setNoUpload(boolean p0) {
}

/** Empty implementation */
public boolean isOnlineToday() {
return false;
}

/** Empty implementation */
public boolean isPendingUpload() {
return false;
}

/** Empty implementation */
public java.lang.String getPlayerBio() {
return null;
}

/** Empty implementation */
public void setPlayerBio(java.lang.String p0) {
}

/** Empty implementation */
public void setFlamingArmor(boolean p0) {
}

/** Empty implementation */
public boolean isGoldCurse() {
return false;
}

/** Empty implementation */
public void setDemonicAura(boolean p0) {
}

/** Empty implementation */
public void setHealthRegen(int p0) {
}

/** Empty implementation */
public long getLogoutTime() {
return 0;
}

/** Empty implementation */
public java.lang.String getLocalPlayerIP() {
return null;
}

/** Empty implementation */
public void setLogoutTime(long p0) {
}

/** Empty implementation */
public long getLastSync() {
return 0;
}

/** Empty implementation */
public void setLastSync(long p0) {
}

/** Empty implementation */
public int getSafeLogout() {
return 0;
}

/** Empty implementation */
public void setLocalPlayerIP(java.lang.String p0) {
}

/** Empty implementation */
public void setSafeLogout(int p0) {
}

/** Empty implementation */
public java.lang.String getToKick() {
return null;
}

/** Empty implementation */
public void setNoobPlayer(boolean p0) {
}

/** Empty implementation */
public void setFatigueEffect(int p0) {
}

/** Empty implementation */
public void setLastOnline(long p0) {
}

/** Empty implementation */
public boolean isFlamingArmor() {
return false;
}

/** Empty implementation */
public int getFatigueEffect() {
return 0;
}

/** Empty implementation */
public float getOldEnergy() {
return 0;
}

/** Empty implementation */
public void setLastAttack(long p0) {
}

/** Empty implementation */
public boolean isStarving() {
return false;
}

/** Empty implementation */
public boolean isNoobPlayer() {
return false;
}

/** Empty implementation */
public void setOnlineToday(boolean p0) {
}

/** Empty implementation */
public void setGoldCurse(boolean p0) {
}

/** Empty implementation */
public int getHealthRegen() {
return 0;
}

/** Empty implementation */
public void setFlameTrail(boolean p0) {
}

/** Empty implementation */
public long getLastAttack() {
return 0;
}

/** Empty implementation */
public java.lang.String getEcashStorage() {
return null;
}

/** Empty implementation */
public void setPendingUpload(boolean p0) {
}

/** Empty implementation */
public boolean isNoUpload() {
return false;
}

/** Empty implementation */
public java.util.List getSavedGear() {
return null;
}

/** Empty implementation */
public void setSavedGear(java.util.List p0) {
}

/** Empty implementation */
public java.lang.String getReportData() {
return null;
}

/** Empty implementation */
public org.bukkit.Location getSavedLocation() {
return null;
}

/** Empty implementation */
public void setSpoofedWeapon(org.bukkit.inventory.ItemStack p0) {
}

/** Empty implementation */
public void setReportData(java.lang.String p0) {
}

/** Empty implementation */
public void setTotalTiming(java.util.List p0) {
}

/** Empty implementation */
public void setMuteCount(int p0) {
}

/** Empty implementation */
public void setServerSwap(java.lang.String p0) {
}

/** Empty implementation */
public int getKickCount() {
return 0;
}

/** Empty implementation */
public boolean isNoNegation() {
return false;
}

/** Empty implementation */
public int getAverageTiming() {
return 0;
}

/** Empty implementation */
public yourfunworldstudios.socialconquer.ItemMechanics.PlayerArrowReplace getArrowReplace() {
return null;
}

/** Empty implementation */
public void setLootSpawnData(java.lang.String p0) {
}

/** Empty implementation */
public void setKickCount(int p0) {
}

/** Empty implementation */
public java.lang.String getServerSwap() {
return null;
}

/** Empty implementation */
public void setAverageTiming(int p0) {
}

/** Empty implementation */
public void setInstanceParty(java.util.List p0) {
}

/** Empty implementation */
public long getLoginTime() {
return 0;
}

/** Empty implementation */
public void setArrowReplace(yourfunworldstudios.socialconquer.ItemMechanics.PlayerArrowReplace p0) {
}

/** Empty implementation */
public void setDodge(int p0) {
}

/** Empty implementation */
public long getLastHit() {
return 0;
}

/** Empty implementation */
public void setArmorContents(java.util.List p0) {
}

/** Empty implementation */
public void setGemFind(int p0) {
}

/** Empty implementation */
public void setDamageData(java.util.List p0) {
}

/** Empty implementation */
public void setBlock(int p0) {
}

/** Empty implementation */
public int getGemFind() {
return 0;
}

/** Empty implementation */
public void setLastHit(long p0) {
}

/** Empty implementation */
public boolean isProcessWeapon() {
return false;
}

/** Empty implementation */
public int getLootSpawnStep() {
return 0;
}

/** Empty implementation */
public void setThorn(int p0) {
}

/** Empty implementation */
public void setLastOrbUse(long p0) {
}

/** Empty implementation */
public void setNeedUpdate(boolean p0) {
}

/** Empty implementation */
public java.lang.String getPlastHit() {
return null;
}

/** Empty implementation */
public void setLootSpawnStep(int p0) {
}

/** Empty implementation */
public java.lang.String getLootSpawnData() {
return null;
}

/** Empty implementation */
public int getReportStep() {
return 0;
}

/** Empty implementation */
public long getLastUnstuck() {
return 0;
}

/** Empty implementation */
public void setLastUnstuck(long p0) {
}

/** Empty implementation */
public int getMuteCount() {
return 0;
}

/** Empty implementation */
public void setProcessWeapon(boolean p0) {
}

/** Empty implementation */
public void setLoginTime(long p0) {
}

/** Empty implementation */
public int getDodge() {
return 0;
}

/** Empty implementation */
public java.util.List getInstanceParty() {
return null;
}

/** Empty implementation */
public int getItemFind() {
return 0;
}

/** Empty implementation */
public long getLastOrbUse() {
return 0;
}

/** Empty implementation */
public void setLoaded(boolean p0) {
}

/** Empty implementation */
public org.bukkit.inventory.ItemStack getSpoofedWeapon() {
return null;
}

/** Empty implementation */
public void setPlastHit(java.lang.String p0) {
}

/** Empty implementation */
public java.lang.String getLostGear() {
return null;
}

/** Empty implementation */
public void setReflection(int p0) {
}

/** Empty implementation */
public int getThorn() {
return 0;
}

/** Empty implementation */
public void setItemFind(int p0) {
}

/** Empty implementation */
public void setNoNegation(boolean p0) {
}

/** Empty implementation */
public int getAlignTime() {
return 0;
}

/** Empty implementation */
public void setAlignTime(int p0) {
}

/** Empty implementation */
public void setLostGear(java.lang.String p0) {
}

/** Empty implementation */
public void setReportStep(int p0) {
}

/** Empty implementation */
public java.util.List getTotalTiming() {
return null;
}

/** Empty implementation */
public void setSavedLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public int getReflection() {
return 0;
}

/** Empty implementation */
public void setArmorData(java.util.List p0) {
}

/** Empty implementation */
public boolean isNeedUpdate() {
return false;
}

/** Empty implementation */
public void setIceResistance(int p0) {
}

/** Empty implementation */
public void setBankContents(java.util.List p0) {
}

/** Empty implementation */
public java.lang.String getPartyLoot() {
return null;
}

/** Empty implementation */
public int getBank() {
return 0;
}

/** Empty implementation */
public int getMobSpawnStep() {
return 0;
}

/** Empty implementation */
public void setWithdraw(int p0) {
}

/** Empty implementation */
public void setBankLevel(int p0) {
}

/** Empty implementation */
public void setPlayerSlow(long p0) {
}

/** Empty implementation */
public org.bukkit.inventory.ItemStack getSummonItem() {
return null;
}

/** Empty implementation */
public void setBanCount(int p0) {
}

/** Empty implementation */
public void setWithdrawType(java.lang.String p0) {
}

/** Empty implementation */
public void setMule(org.bukkit.entity.Entity p0) {
}

/** Empty implementation */
public long getPlayerSlow() {
return 0;
}

/** Empty implementation */
public java.lang.String getPartyInvite() {
return null;
}

/** Empty implementation */
public boolean isPartyOnly() {
return false;
}

/** Empty implementation */
public void setPartyOnly(boolean p0) {
}

/** Empty implementation */
public java.util.List getPets() {
return null;
}

/** Empty implementation */
public void setPetSpawnDelay(long p0) {
}

/** Empty implementation */
public java.util.List getPetEntities() {
return null;
}

/** Empty implementation */
public org.bukkit.Location getNamingPet() {
return null;
}

/** Empty implementation */
public void setPets(java.util.List p0) {
}

/** Empty implementation */
public java.util.List getPhraseList() {
return null;
}

/** Empty implementation */
public org.bukkit.inventory.Inventory getMuleInventory() {
return null;
}

/** Empty implementation */
public long getPetSpawnDelay() {
return 0;
}

/** Empty implementation */
public void setPhraseList(java.util.List p0) {
}

/** Empty implementation */
public int getSplit() {
return 0;
}

/** Empty implementation */
public void setSummonItem(org.bukkit.inventory.ItemStack p0) {
}

/** Empty implementation */
public void setVanished(boolean p0) {
}

/** Empty implementation */
public java.util.List getBankContents() {
return null;
}

/** Empty implementation */
public int getSlowMining() {
return 0;
}

/** Empty implementation */
public void setSlowMining(int p0) {
}

/** Empty implementation */
public long getLastSwing() {
return 0;
}

/** Empty implementation */
public boolean isVanished() {
return false;
}

/** Empty implementation */
public int getSummonMount() {
return 0;
}

/** Empty implementation */
public void setLastSwing(long p0) {
}

/** Empty implementation */
public org.bukkit.entity.Entity getMule() {
return null;
}

/** Empty implementation */
public java.lang.String getMuleItemList() {
return null;
}

/** Empty implementation */
public void setUsedStuck(boolean p0) {
}

/** Empty implementation */
public void setMobSpawnData(java.lang.String p0) {
}

/** Empty implementation */
public int getBankLevel() {
return 0;
}

/** Empty implementation */
public void setSummonMount(int p0) {
}

/** Empty implementation */
public void setBank(int p0) {
}

/** Empty implementation */
public void setMobSpawnStep(int p0) {
}

/** Empty implementation */
public void setMuleInventory(org.bukkit.inventory.Inventory p0) {
}

/** Empty implementation */
public void setPetEntities(java.util.List p0) {
}

/** Empty implementation */
public void setSplit(int p0) {
}

/** Empty implementation */
public void setPetType(java.lang.String p0) {
}

/** Empty implementation */
public int getWithdraw() {
return 0;
}

/** Empty implementation */
public void setNamingPet(org.bukkit.Location p0) {
}

/** Empty implementation */
public java.lang.String getPetType() {
return null;
}

/** Empty implementation */
public void setMount(org.bukkit.entity.Entity p0) {
}

/** Empty implementation */
public void setParty(yourfunworldstudios.socialconquer.PartyMechanics.Party p0) {
}

/** Empty implementation */
public boolean isUsedStuck() {
return false;
}

/** Empty implementation */
public void setInShop(boolean p0) {
}

/** Empty implementation */
public java.lang.String getPassiveHunter() {
return null;
}

/** Empty implementation */
public void setPassiveHunter(java.lang.String p0) {
}

/** Empty implementation */
public int getBanCount() {
return 0;
}

/** Empty implementation */
public void setPartyLoot(java.lang.String p0) {
}

/** Empty implementation */
public java.lang.String getWithdrawType() {
return null;
}

/** Empty implementation */
public java.lang.String getMobSpawnData() {
return null;
}

/** Empty implementation */
public org.bukkit.entity.Entity getMount() {
return null;
}

/** Empty implementation */
public void setMuleItemList(java.lang.String p0) {
}

/** Empty implementation */
public yourfunworldstudios.socialconquer.PartyMechanics.Party getParty() {
return null;
}

/** Empty implementation */
public void setPartyInvite(java.lang.String p0) {
}

/** Empty implementation */
public boolean isInShop() {
return false;
}

/** Empty implementation */
public void setRecentCraft(boolean p0) {
}

/** Empty implementation */
public void setNeedSQLUpdate(boolean p0) {
}

/** Empty implementation */
public java.lang.String getTp() {
return null;
}

/** Empty implementation */
public void setHasPortal(boolean p0) {
}

/** Empty implementation */
public void setBuildList(java.util.List p0) {
}

/** Empty implementation */
public void setOpeningShop(boolean p0) {
}

/** Empty implementation */
public int getMobKills() {
return 0;
}

/** Empty implementation */
public void setGodMode(long p0) {
}

/** Empty implementation */
public void setCorruptWorld(boolean p0) {
}

/** Empty implementation */
public void setTp(java.lang.String p0) {
}

/** Empty implementation */
public void setZoneType(java.lang.String p0) {
}

/** Empty implementation */
public void setMobKills(int p0) {
}

/** Empty implementation */
public int getRepairState() {
return 0;
}

/** Empty implementation */
public void setRealmPercent(java.lang.String p0) {
}

/** Empty implementation */
public long getLastShopOpen() {
return 0;
}

/** Empty implementation */
public long getFlyingRealm() {
return 0;
}

/** Empty implementation */
public void setShopCurrency(java.lang.String p0) {
}

/** Empty implementation */
public boolean isCorruptWorld() {
return false;
}

/** Empty implementation */
public boolean isReadyWorld() {
return false;
}

/** Empty implementation */
public org.bukkit.inventory.ItemStack getRepair() {
return null;
}

/** Empty implementation */
public void setShopPage(int p0) {
}

/** Empty implementation */
public void setSavedLevels(int p0) {
}

/** Empty implementation */
public int getRealmTier() {
return 0;
}

/** Empty implementation */
public void setSafeRealm(long p0) {
}

/** Empty implementation */
public void setAnvil(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setRealmTier(int p0) {
}

/** Empty implementation */
public int getShopPage() {
return 0;
}

/** Empty implementation */
public long getGodMode() {
return 0;
}

/** Empty implementation */
public int getDeaths() {
return 0;
}

/** Empty implementation */
public void setRepair(org.bukkit.inventory.ItemStack p0) {
}

/** Empty implementation */
public void setRepairState(int p0) {
}

/** Empty implementation */
public java.lang.String getRealmTitle() {
return null;
}

/** Empty implementation */
public java.lang.String getZoneType() {
return null;
}

/** Empty implementation */
public boolean isRecentCraft() {
return false;
}

/** Empty implementation */
public void setShopLevel(int p0) {
}

/** Empty implementation */
public void setShopServer(int p0) {
}

/** Empty implementation */
public org.bukkit.inventory.Inventory getCollectionBin() {
return null;
}

/** Empty implementation */
public void setCollectionBin(org.bukkit.inventory.Inventory p0) {
}

/** Empty implementation */
public long getSafeRealm() {
return 0;
}

/** Empty implementation */
public void setFlyingRealm(long p0) {
}

/** Empty implementation */
public boolean isNeedSQLUpdate() {
return false;
}

/** Empty implementation */
public void setReadyWorld(boolean p0) {
}

/** Empty implementation */
public java.lang.String getRealmPercent() {
return null;
}

/** Empty implementation */
public void setInInventory(boolean p0) {
}

/** Empty implementation */
public boolean isInInventory() {
return false;
}

/** Empty implementation */
public org.bukkit.inventory.Inventory getShopStock() {
return null;
}

/** Empty implementation */
public void setDeaths(int p0) {
}

/** Empty implementation */
public java.util.List getBuildList() {
return null;
}

/** Empty implementation */
public int getSavedLevels() {
return 0;
}

/** Empty implementation */
public java.util.List getKills() {
return null;
}

/** Empty implementation */
public java.lang.String getShopCurrency() {
return null;
}

/** Empty implementation */
public org.bukkit.entity.Item getItemRepair() {
return null;
}

/** Empty implementation */
public void setItemRepair(org.bukkit.entity.Item p0) {
}

/** Empty implementation */
public int getShopLevel() {
return 0;
}

/** Empty implementation */
public void setInverseShop(org.bukkit.block.Block p0) {
}

/** Empty implementation */
public void setShopStock(org.bukkit.inventory.Inventory p0) {
}

/** Empty implementation */
public int getShopServer() {
return 0;
}

/** Empty implementation */
public void setLastShopOpen(long p0) {
}

/** Empty implementation */
public void setKills(java.util.List p0) {
}

/** Empty implementation */
public boolean isOpeningShop() {
return false;
}

/** Empty implementation */
public org.bukkit.block.Block getInverseShop() {
return null;
}

/** Empty implementation */
public void setRealmTitle(java.lang.String p0) {
}

/** Empty implementation */
public boolean isHasPortal() {
return false;
}

/** Empty implementation */
public org.bukkit.Location getAnvil() {
return null;
}

/** Empty implementation */
public void setQuest(java.util.List p0) {
}

/** Empty implementation */
public boolean isSkipConfirm() {
return false;
}

/** Empty implementation */
public org.bukkit.entity.Player getTradePartners() {
return null;
}

/** Empty implementation */
public void setWeather(org.bukkit.WeatherType p0) {
}

/** Empty implementation */
public void setBuyingItem(boolean p0) {
}

/** Empty implementation */
public void setLeaveConfirm(boolean p0) {
}

/** Empty implementation */
public org.bukkit.Location getTpLocation() {
return null;
}

/** Empty implementation */
public org.bukkit.inventory.Inventory getTradeSecure() {
return null;
}

/** Empty implementation */
public void setEnchantScroll(boolean p0) {
}

/** Empty implementation */
public void setTpLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setTradeSecure(org.bukkit.inventory.Inventory p0) {
}

/** Empty implementation */
public void setSkipConfirm(boolean p0) {
}

/** Empty implementation */
public java.util.List getQuest() {
return null;
}

/** Empty implementation */
public int getTpEffect() {
return 0;
}

/** Empty implementation */
public boolean isOnIsland() {
return false;
}

/** Empty implementation */
public boolean isLeaveConfirm() {
return false;
}

/** Empty implementation */
public void setOnIsland(boolean p0) {
}

/** Empty implementation */
public org.bukkit.WeatherType getWeather() {
return null;
}

/** Empty implementation */
public void setTpEffect(int p0) {
}

/** Empty implementation */
public org.bukkit.entity.Player getTrade() {
return null;
}

/** Empty implementation */
public void setTrade(org.bukkit.entity.Player p0) {
}

/** Empty implementation */
public void setTradePartners(org.bukkit.entity.Player p0) {
}

/** Empty implementation */
public org.bukkit.Location getWarp() {
return null;
}

/** Empty implementation */
public void setWarp(org.bukkit.Location p0) {
}

/** Empty implementation */
public int getIceResistance() {
return 0;
}

/** Empty implementation */
public void setStrength(int p0) {
}

/** Empty implementation */
public void setPlayerSDaysLeft(int p0) {
}

/** Empty implementation */
public long getInstanceTiming() {
return 0;
}

/** Empty implementation */
public java.util.List getPlayerPortalShards() {
return null;
}

/** Empty implementation */
public org.bukkit.Location getMusicBoxLocation() {
return null;
}

/** Empty implementation */
public long getDestructionWandLastUse() {
return 0;
}

/** Empty implementation */
public org.bukkit.Location getHearthstoneLocation() {
return null;
}

/** Empty implementation */
public java.util.List getRemotePlayerData() {
return null;
}

/** Empty implementation */
public void setForumUserGroup(int p0) {
}

/** Empty implementation */
public void setServerSwapLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setInstanceTiming(long p0) {
}

/** Empty implementation */
public java.lang.String getPlayerInstance() {
return null;
}

/** Empty implementation */
public long getPlayerFirstLogin() {
return 0;
}

/** Empty implementation */
public void setSavedLocationInstance(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setPlayerLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public long getDestructionWandCooldown() {
return 0;
}

/** Empty implementation */
public int getPlayerFoodLevel() {
return 0;
}

/** Empty implementation */
public org.bukkit.inventory.ItemStack[] getPlayerArmorContents() {
return null;
}

/** Empty implementation */
public int getHearthstoneTimer() {
return 0;
}

/** Empty implementation */
public void setPlayerInventory(org.bukkit.inventory.Inventory p0) {
}

/** Empty implementation */
public void setPlayerArmorContents(org.bukkit.inventory.ItemStack[] p0) {
}

/** Empty implementation */
public int getNoobPlayerWarning() {
return 0;
}

/** Empty implementation */
public void setDestructionWandLastUse(long p0) {
}

/** Empty implementation */
public void setHearthstoneLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setRemotePlayerData(java.util.List p0) {
}

/** Empty implementation */
public org.bukkit.inventory.Inventory getPlayerInventory() {
return null;
}

/** Empty implementation */
public void setPlayerItemInHand(org.bukkit.inventory.ItemStack p0) {
}

/** Empty implementation */
public org.bukkit.inventory.Inventory getPlayerMuleInventory() {
return null;
}

/** Empty implementation */
public void setHearthstoneTimer(int p0) {
}

/** Empty implementation */
public int getDestructionWandUseCount() {
return 0;
}

/** Empty implementation */
public org.bukkit.Location getServerSwapLocation() {
return null;
}

/** Empty implementation */
public java.lang.String getServerSwapPending() {
return null;
}

/** Empty implementation */
public void setPlayerInstance(java.lang.String p0) {
}

/** Empty implementation */
public void setLastHitLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public org.bukkit.Location getSavedLocationInstance() {
return null;
}

/** Empty implementation */
public long getProcessingMove() {
return 0;
}

/** Empty implementation */
public void setDestructionWandUseCount(int p0) {
}

/** Empty implementation */
public long getMusicBoxCooldown() {
return 0;
}

/** Empty implementation */
public org.bukkit.Location getLastHitLocation() {
return null;
}

/** Empty implementation */
public int getPlayerSDaysLeft() {
return 0;
}

/** Empty implementation */
public void setPlayerFirstLogin(long p0) {
}

/** Empty implementation */
public void setProcessingMove(long p0) {
}

/** Empty implementation */
public void setFireResistance(int p0) {
}

/** Empty implementation */
public void setPoisonResistance(int p0) {
}

/** Empty implementation */
public long getLastEnvironmentalDamage() {
return 0;
}

/** Empty implementation */
public void setLastEnvironmentalDamage(long p0) {
}

/** Empty implementation */
public void setNoobPlayerWarning(int p0) {
}

/** Empty implementation */
public org.bukkit.Location getPlayerLocation() {
return null;
}

/** Empty implementation */
public void setEnergyRegenData(float p0) {
}

/** Empty implementation */
public void setMusicBoxPlacement(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setPlayerPortalShards(java.util.List p0) {
}

/** Empty implementation */
public void setDestructionWandCooldown(long p0) {
}

/** Empty implementation */
public void setPlayerFoodLevel(int p0) {
}

/** Empty implementation */
public void setPlayerMuleInventory(org.bukkit.inventory.Inventory p0) {
}

/** Empty implementation */
public int getForumUserGroup() {
return 0;
}

/** Empty implementation */
public void setMusicBoxCooldown(long p0) {
}

/** Empty implementation */
public float getEnergyRegenData() {
return 0;
}

/** Empty implementation */
public void setServerSwapPending(java.lang.String p0) {
}

/** Empty implementation */
public org.bukkit.inventory.ItemStack getPlayerItemInHand() {
return null;
}

/** Empty implementation */
public void setMusicBoxLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public org.bukkit.Location getMusicBoxPlacement() {
return null;
}

/** Empty implementation */
public void setLookingIntoOfflineBank(java.lang.String p0) {
}

/** Empty implementation */
public void setParticleEffects(int p0) {
}

/** Empty implementation */
public void setItemBeingBought(int p0) {
}

/** Empty implementation */
public int getFishCaughtCount() {
return 0;
}

/** Empty implementation */
public void setMobSpawnLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public int getFishEnergyRegen() {
return 0;
}

/** Empty implementation */
public java.lang.String getLookingIntoOfflineBank() {
return null;
}

/** Empty implementation */
public java.lang.String getBankUpgradeCode() {
return null;
}

/** Empty implementation */
public void setLastPlayerAttack(long p0) {
}

/** Empty implementation */
public void setEcashItemBeingBought(int p0) {
}

/** Empty implementation */
public void setShardItemBeingBought(int p0) {
}

/** Empty implementation */
public void setProcessingDamageEvent(boolean p0) {
}

/** Empty implementation */
public int getParticleEffects() {
return 0;
}

/** Empty implementation */
public int getShardItemBeingBought() {
return 0;
}

/** Empty implementation */
public int getDyeBeingBought() {
return 0;
}

/** Empty implementation */
public org.bukkit.Location getMobSpawnLocation() {
return null;
}

/** Empty implementation */
public int getMountBeingBought() {
return 0;
}

/** Empty implementation */
public void setDyeBeingBought(int p0) {
}

/** Empty implementation */
public void setMountBeingBought(int p0) {
}

/** Empty implementation */
public long getLastMobMessage() {
return 0;
}

/** Empty implementation */
public int getItemBeingBought() {
return 0;
}

/** Empty implementation */
public int getOrbBeingBought() {
return 0;
}

/** Empty implementation */
public boolean isProcessingDamageEvent() {
return false;
}

/** Empty implementation */
public void setLootSpawnLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public org.bukkit.Location getSummonLocation() {
return null;
}

/** Empty implementation */
public void setPartyLootIndex(int p0) {
}

/** Empty implementation */
public void setRankForumGroup(int p0) {
}

/** Empty implementation */
public org.bukkit.Location getHorseSageLocation() {
return null;
}

/** Empty implementation */
public void setPlayerFishingSpot(org.bukkit.Location p0) {
}

/** Empty implementation */
public java.util.HashMap getFurnaceInventory() {
return null;
}

/** Empty implementation */
public void setFurnaceInventory(java.util.HashMap p0) {
}

/** Empty implementation */
public boolean isIgnoreFurnaceOpenEvent() {
return false;
}

/** Empty implementation */
public int getSkillBeingBought() {
return 0;
}

/** Empty implementation */
public void setFishCaughtCount(int p0) {
}

/** Empty implementation */
public void setOrbBeingBought(int p0) {
}

/** Empty implementation */
public int getFishHealthRegen() {
return 0;
}

/** Empty implementation */
public void setSkillBeingBought(int p0) {
}

/** Empty implementation */
public org.bukkit.Location getLootSpawnLocation() {
return null;
}

/** Empty implementation */
public long getPartyInviteTime() {
return 0;
}

/** Empty implementation */
public void setFishHealthRegen(int p0) {
}

/** Empty implementation */
public int getEcashItemBeingBought() {
return 0;
}

/** Empty implementation */
public void setProcessingProjectileEvent(boolean p0) {
}

/** Empty implementation */
public void setFishEnergyRegen(int p0) {
}

/** Empty implementation */
public int getFishBonusDamage() {
return 0;
}

/** Empty implementation */
public void setFishBonusDamage(int p0) {
}

/** Empty implementation */
public void setFishBonusArmor(int p0) {
}

/** Empty implementation */
public void setLastOpenedLootChest(org.bukkit.Location p0) {
}

/** Empty implementation */
public int getFishBonusArmor() {
return 0;
}

/** Empty implementation */
public int getPartyLootIndex() {
return 0;
}

/** Empty implementation */
public boolean isProcessingProjectileEvent() {
return false;
}

/** Empty implementation */
public void setHorseSageLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public org.bukkit.Location getPlayerFishingSpot() {
return null;
}

/** Empty implementation */
public int getFishBonusBlock() {
return 0;
}

/** Empty implementation */
public int getRankForumGroup() {
return 0;
}

/** Empty implementation */
public void setSummonLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setFishBonusBlock(int p0) {
}

/** Empty implementation */
public void setLastMobMessage(long p0) {
}

/** Empty implementation */
public int getFishBonusLifesteal() {
return 0;
}

/** Empty implementation */
public void setPartyInviteTime(long p0) {
}

/** Empty implementation */
public void setFishBonusLifesteal(int p0) {
}

/** Empty implementation */
public long getLastPlayerAttack() {
return 0;
}

/** Empty implementation */
public void setBankUpgradeCode(java.lang.String p0) {
}

/** Empty implementation */
public void setIgnoreFurnaceOpenEvent(boolean p0) {
}

/** Empty implementation */
public org.bukkit.Location getLastOpenedLootChest() {
return null;
}

/** Empty implementation */
public void setDuelStatistics(java.util.List p0) {
}

/** Empty implementation */
public long getRecentMovement() {
return 0;
}

/** Empty implementation */
public org.bukkit.Location getInventoryPortalMap() {
return null;
}

/** Empty implementation */
public void setCurrentItemBeingBought(int p0) {
}

/** Empty implementation */
public void setRealmLoadedStatus(boolean p0) {
}

/** Empty implementation */
public long getRealmResetCooldown() {
return 0;
}

/** Empty implementation */
public int getInventoryUpdate() {
return 0;
}

/** Empty implementation */
public void setInventoryUpdate(int p0) {
}

/** Empty implementation */
public void setShopBeingBrowsed(java.lang.String p0) {
}

/** Empty implementation */
public void setProcessingMoveTeleport(long p0) {
}

/** Empty implementation */
public boolean isRealmLoadedStatus() {
return false;
}

/** Empty implementation */
public long getLastInventoryClose() {
return 0;
}

/** Empty implementation */
public void setRealmUpgradeCode(java.lang.String p0) {
}

/** Empty implementation */
public java.util.List getDuelStatistics() {
return null;
}

/** Empty implementation */
public void setRecentMovement(long p0) {
}

/** Empty implementation */
public int getCurrentItemBeingBought() {
return 0;
}

/** Empty implementation */
public void setSavedOutOfRealmLocation(org.bukkit.Location p0) {
}

/** Empty implementation */
public java.lang.String getPortalMapCoords() {
return null;
}

/** Empty implementation */
public void setRealmResetCooldown(long p0) {
}

/** Empty implementation */
public boolean isRecentBlockEvent() {
return false;
}

/** Empty implementation */
public void setItemBeingStocked(org.bukkit.inventory.ItemStack p0) {
}

/** Empty implementation */
public org.bukkit.inventory.ItemStack getItemBeingStocked() {
return null;
}

/** Empty implementation */
public void setRecentBlockEvent(boolean p0) {
}

/** Empty implementation */
public java.lang.String getShopBeingBrowsed() {
return null;
}

/** Empty implementation */
public void setCompletionDelay(java.util.List p0) {
}

/** Empty implementation */
public void setShopUpgradeCode(java.lang.String p0) {
}

/** Empty implementation */
public long getWarnedDurability() {
return 0;
}

/** Empty implementation */
public long getPortalCooldown() {
return 0;
}

/** Empty implementation */
public void setLastInventoryClose(long p0) {
}

/** Empty implementation */
public int getFishBonusCriticalHit() {
return 0;
}

/** Empty implementation */
public void setInventoryPortalMap(org.bukkit.Location p0) {
}

/** Empty implementation */
public void setPriceUpdateNeeded(boolean p0) {
}

/** Empty implementation */
public void setWarnedDurability(long p0) {
}

/** Empty implementation */
public void setFishBonusCriticalHit(int p0) {
}

/** Empty implementation */
public java.util.List getCompletionDelay() {
return null;
}

/** Empty implementation */
public void setPortalCooldown(long p0) {
}

/** Empty implementation */
public boolean isPriceUpdateNeeded() {
return false;
}

/** Empty implementation */
public void setPortalMapCoords(java.lang.String p0) {
}

/** Empty implementation */
public org.bukkit.Location getSavedOutOfRealmLocation() {
return null;
}

/** Empty implementation */
public java.lang.String getRealmUpgradeCode() {
return null;
}

/** Empty implementation */
public java.lang.String getShopUpgradeCode() {
return null;
}

/** Empty implementation */
public long getProcessingMoveTeleport() {
return 0;
}

/** Empty implementation */
public void setTier(int p0) {
}

}
